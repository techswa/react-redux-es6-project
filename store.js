import { applyMiddleware, createStore } from "redux";
// import { composeWithDevTools } from 'redux-devtools-extension';

// import axios from "axios";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware"
import rootReducer from "./reducers";

// const middleware = applyMiddleware(promise(), thunk, logger);


const store = createStore(rootReducer, 
    applyMiddleware(promise(), thunk, logger)
  );

export default store;

// const middleware = applyMiddleware(promise(), thunk, logger);



// export default createStore(rootReducer,middleware);
