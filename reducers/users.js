
// export const 

import { FETCH_USER } from '../actions/actionTypes';
import { SET_USER_SUCCESS_ACCESS_TOKEN } from '../actions/actionTypes';
import { SET_USER_SUCCESS_USER_ID } from '../actions/actionTypes';
import { FETCH_USER_BUSY } from '../actions/actionTypes';
import { FETCH_USER_REJECTED } from '../actions/actionTypes';
import { USER_LOGOUT } from '../actions/actionTypes';
import { SET_USER_ID } from '../actions/actionTypes';
import { SET_USER_ROLE } from '../actions/actionTypes';
import { SET_TECH_ID } from '../actions/actionTypes';
import { SET_ACCESS_TOKEN } from '../actions/actionTypes';
import { SET_USER_EMAIL } from '../actions/actionTypes';
import { SET_USER_PASSWORD } from '../actions/actionTypes';
import { SET_CURRENT_PAGE } from '../actions/actionTypes';
import { SET_CURRENT_LAYOUT } from '../actions/actionTypes';
import { SEARCH_PROPERTIES } from '../actions/actionTypes';
import { PROPERTY_RESULTS_TABLE } from '../actions/actionTypes';
import { PROPERTY_RESULTS_SUMMARY } from '../actions/actionTypes';





// export const 
const initialState = {users:[
    { 
    user_role: 'FRONT',
    user_id:'JH',
    user_email:'jsmaith@yahoo.com.com',
    user_password:'P3s8a',
    access_token:'y43y4yrehrhreh',
    fetch_user_busy: false,
    fetch_user_success: false
    },{
    user_role: 'TECH',
    user_id:'KE',
    user_email:'sample@swbell.net',
    user_password:'33F8a',
    access_token:'64w44egw4',
    fetch_user_busy: false,
    fetch_user_success: false
    }
],
current_page: SEARCH_PROPERTIES,
current_layout:PROPERTY_RESULTS_SUMMARY
};


export default function users( 
    state = initialState , action) 
    {
    switch (action.type) {
        case FETCH_USER: {
            //return {...state, fetching: true }
            return state;
        }
        case SET_USER_SUCCESS_ACCESS_TOKEN: {
            // console.log("action payload");
            return {...state, access_token: action.payload}
        }
        case SET_USER_SUCCESS_USER_ID: {
            return {...state, user_id: action.payload}
        }
        case FETCH_USER_BUSY: {
            return {...state, fetchingUser: true }
        }
        case FETCH_USER_REJECTED: {
            return {...state, fetchingUser: false, error: action.payload }
        }
        case USER_LOGOUT: {
            return {...state, user_id: action.payload, access_token: action.payload}
        }
        case SET_USER_ID: {
            return {...state, user_id: action.payload}
        }
        case SET_USER_ROLE: {
            return {...state, user_role: action.payload}
        }
        case SET_TECH_ID: {
            return {...state, access_token: action.payload}
        }
        case SET_ACCESS_TOKEN: {
            return {...state, access_token: action.payload}
        }
        case SET_USER_EMAIL: {
            return {...state, user_email: action.payload}
        }
        case SET_USER_PASSWORD: {
            return {...state, user_password: action.payload}
        }
        case SET_CURRENT_PAGE: {
            return {...state, current_page: action.payload}
        }
        case SET_CURRENT_LAYOUT: {
            return {...state, current_layout: action.payload}
        }
        default:{
            return state;
        }
    }
}