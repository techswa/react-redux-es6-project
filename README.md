# ReactJS & Redux Javascript ES6 application extraction#

This source code has been extracted from an actual project and has been modified slightly.  
These files demonstrate the use of ReactJS and Javascript ES6 features listed below among other items.

*  ReactJS
*  Redux
*  Redux-Thunk
*  Javascript ES6
*  AJAX
*  JSON
*  Map
*  Filter
*  Spread
*  Composition
*  DOM maniputlation
*  State Machines
*  Promises
*  Async I/O