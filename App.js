import React from 'react';
// eslint-disable-next-line
import { BrowserRouter, Route, NavLink, Switch, Redirect } from 'react-router-dom';
// import HomePage from './components/pages/HomePage';
import IndexLayout from './components/layouts/IndexLayout';
// import MiningLayout from './components/layouts/MiningLayout';
// import TicketsPage from './components/pages/TicketsPage';

//<Route path="/mining" component={MiningLayout} />
const App = () => (
  <div className="container-float">
     <Switch> 
      
      <Route path="/" exact component={IndexLayout} />
      <Redirect to="/" />
    </Switch>  
  </div>
);

export default App;
