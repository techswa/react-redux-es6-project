import React from 'react';
import { connect } from "react-redux";
import { BrowserRouter, Route, NavLink } from 'react-router-dom';
import { Redirect } from 'react-router';

import Table from 'react-bootstrap/lib/Table';
import Row from 'react-bootstrap/lib/Row';

import { addPropertyToProspects } from '../../actions/prospectActions';
import { removePropertyFromProspects } from '../../actions/prospectActions';

import COLOR_SELECTED_RECORD from '../../libs/Constants'

class ProspectsTable extends React.Component {

//These wil be the columns processed for diplay
//Bootstrap tabl eis automatically generated based upon the entries below
columnsToDisplay = () => {
return [['account_num', 'Account'], ['block','Block'], ['street','Street']
,['age','Age'], ['bedrooms', 'Bedrooms'], ['full_baths', 'Bathrooms']
,['condition', 'Condition'], ['depreciation_pct', 'Depreciation'] ]
}

columnTitles = () => {
  let _columnTitles = [];
  const _columnsToDisplay = this.columnsToDisplay();
  _columnTitles = _columnsToDisplay.map(entry => entry[1]) 
  return _columnTitles
}

createTableHeader = () => {
  const columnTitles= this.columnTitles();
  let _columnTitles = []
  _columnTitles = columnTitles.map(title => <th>{title}</th>) 
  return <thead><tr>{_columnTitles}</tr></thead>
}

createCheckBox = (dataRowId) => {
  const checkBoxStyle = {'margin-top':0};
  const _checkboxId = 'checkbox-' + dataRowId;
  return <Checkbox unchecked="true" id={_checkboxId} className={checkBoxStyle} onClick={this.handleClick.bind(this)}></Checkbox>
}

//Set background color on row
toggleSelectedRowHighlight = (row_id) => {

}

//Add record to the prospect
//User clicks on row and this is called
addPropertyToprospect = (property_id) => {


}




columnFields = () => {
  let _columnData = [];
  const _columnsToDisplay = this.columnsToDisplay();
  _columnData = _columnsToDisplay.map(entry => entry[0]) 
  return _columnData
}

//******************/
createTableRows = () => {
  let _row = []
  let _rowTemplate = []
  let _fieldTemplate = []
  // let _checkbox = this.createCheckBox();
  let _dataRowId = '0';
  const columnFields = this.columnFields(); //["account_num", "block", "street", "age", "bedrooms", "bathrooms", "condition", "depreciation"]
  const prospects = this.props.prospects;
  // let property = properties[0]


// Makes row based on columnFields
_row = prospects.map((prospect, i)  => {
  //Set data row for click on table
  
  _dataRowId = prospect['id']
  _row = columnFields.map((fieldName, index) => {
    _fieldTemplate.push(<td>{prospect[fieldName]}</td>)   
  })

  _rowTemplate.push(<tr onClick={this.handleClick.bind(this)} data-row-id={_dataRowId} >{_fieldTemplate}</tr>)
  _fieldTemplate = [] //Reset for new row

});

return _rowTemplate

}

createTableFullWidth = () =>{
  return(
    <Table striped condensed hover>
      {this.createTableHeader()}
      <tbody>
        {this.createTableRows()}
      </tbody>
    </Table>
  )
}





handleClick(e) {
 
  // If the row was clicked process here
  const eventElement = e.srcElement || e.target
  const nodeName = eventElement.parentNode.nodeName
  console.log("nodeName: " + nodeName)
  const dataRowId = eventElement.parentNode.getAttribute('data-row-id')



  if (nodeName == 'TR'){
    //Toggle the row's check box
    console.log('Row: ' + dataRowId) 

  } 
  // targetCheckbox.checked=true;
  // targetCheckbox.checked != targetCheckbox.checked;
  switch (e.target.id) {

      default:{
          return ;
      }
  }
} 


  render() {
    const {prospects} = this.props.prospects;
    const users = this.props.users;
    const displayTableFullWidth = this.createTableFullWidth();
    

   return (
            <div>  
              {displayTableFullWidth}
            </div>
    
   );
 }
}      
 

export default connect(state => ({session:state.session, users: state.users, prospects: state.prospects } ))(ProspectsTable);
 