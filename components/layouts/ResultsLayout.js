import React from "react";

import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';

import Tab from 'react-bootstrap/lib/Tab';
import Tabs from 'react-bootstrap/lib/Tabs';

import QueryFormHorizontal from '../partials/QueryFormHorizontal'

import { LIST_VIEW } from '../../actions/actionTypes';                  
import { DETAIL_VIEW } from '../../actions/actionTypes';   

import PropertyTable from '../partials/PropertyTable'

class ResultsLayout extends React.Component {

  tabResults = () => {
    return(
      <Tabs
         onSelect={this.handleSelect}
        id="tab-results"
      >
        <Tab eventKey={LIST_VIEW} title="List">
          <PropertyTable/>
        </Tab>

        <Tab eventKey={DETAIL_VIEW} title="Detail">
          Detail View
        </Tab>

      </Tabs>
    )

  }
  render(){
    const tabResults = this.tabResults();
  return(
    <div>
      {tabResults}
    </div>
  );
  }
 
}
export default ResultsLayout;
