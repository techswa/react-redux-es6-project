
import React from 'react';
import { connect } from "react-redux";


import Navbar from 'react-bootstrap/lib/Navbar';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import { setCurrentLayout } from '../../actions/userActions';

import { INDEX_LAYOUT } from '../../actions/actionTypes';
import { MINING_LAYOUT } from '../../actions/actionTypes';
import { RESULTS_LAYOUT } from '../../actions/actionTypes';
import { PROSPECTS_LAYOUT } from '../../actions/actionTypes';

class NavBarMain extends React.Component {

  handleSelect(e) {
    console.log(e);

    switch (e) {

      case INDEX_LAYOUT: {
        this.props.dispatch(setCurrentLayout(e));
        return 
      }
      case MINING_LAYOUT: {
        this.props.dispatch(setCurrentLayout(e));
        return 
      }
      case RESULTS_LAYOUT: {
        this.props.dispatch(setCurrentLayout(e));
        return 
      }
      case PROSPECTS_LAYOUT: {
        this.props.dispatch(setCurrentLayout(e));
        return 
      }
      default:{
        return ;
      }
    } 
 }

  render() {

    return (
        <div>
          <Navbar>
            <Navbar.Header>
              <Navbar.Brand>
                <a href="#">TBD</a>
              </Navbar.Brand>
            </Navbar.Header>
            <Nav>
              <NavItem eventKey={INDEX_LAYOUT} href="#" onSelect={this.handleSelect.bind(this)}>
                Index
              </NavItem>
              <NavItem eventKey={MINING_LAYOUT} href="#" onSelect={this.handleSelect.bind(this)}>
                Mining
              </NavItem>
              <NavItem eventKey={RESULTS_LAYOUT} href="#" onSelect={this.handleSelect.bind(this)}>
                Results
              </NavItem>
              <NavItem eventKey={PROSPECTS_LAYOUT} href="#" onSelect={this.handleSelect.bind(this)}>
                Prospects
              </NavItem>
            </Nav>
          </Navbar>
        </div>
      );
    }
  } 
export default connect(state => ({properties: state.properties}))(NavBarMain);