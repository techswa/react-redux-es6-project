import React from 'react';
import { connect } from "react-redux";

import { SELECT_YEAR_BUILT_EARLIEST, SELECT_YEAR_BUILT_LATEST, ANY } from '../../libs/Constants';

import FormGroup from 'react-bootstrap/lib/FormGroup';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormControl from 'react-bootstrap/lib/FormControl';
import { setFilterYearBuiltEarliest, setFilterYearBuiltLatest } from '../../actions/filterActions';


class SelectYearBuilt extends React.Component {

    constructor(props){
        super(props)
    }

    createIntSequenceDesc = (min, max) => {
        let count = max - min 
        let sequence = Array.from(new Array(count), (x,i) => max - i)
        return sequence;
      }

    handleSelect(e) {

        switch (e.target.id) {
            case SELECT_YEAR_BUILT_EARLIEST: {
                this.props.dispatch(setFilterYearBuiltEarliest(e.target.value));
                return 
            }
            case SELECT_YEAR_BUILT_LATEST: {
                this.props.dispatch(setFilterYearBuiltLatest(e.target.value));
                return 
            }
            default:{
                return ;
            }
        }
    } 

    render() {
        let commonDateRange = this.createIntSequenceDesc(1900, 2018);
        const selectBsSize = this.props.bsSize;
        const selectBsStyle = this.props.bsClass;
        const year_built_earliest = this.props.filters.year_built_earliest
        const year_built_latest = this.props.filters.year_built_latest    

       return ( 
        <div>   
            <FormGroup controlId={SELECT_YEAR_BUILT_EARLIEST} bsSize={selectBsSize} className={selectBsStyle}>
                <ControlLabel>Earliest Built</ControlLabel>
                <FormControl componentClass="select" placeholder="select" onChange={this.handleSelect.bind(this)} value={year_built_earliest}>
                    <option key={'sybea0'} value={ANY}>Any</option>    
                    {commonDateRange.map(x => <option key={'sybe'+ x} value={x}>{x}</option>)}
                </FormControl>
            </FormGroup>
            
            <FormGroup controlId={SELECT_YEAR_BUILT_LATEST} bsSize={selectBsSize} className={selectBsStyle}>  
                <ControlLabel>Latest Built </ControlLabel>
                <FormControl componentClass="select" placeholder="select" onChange={this.handleSelect.bind(this)} value={year_built_latest}>
                    <option key={'sybla0'} value={ANY}>Any</option>    
                    {commonDateRange.map(x => <option key={'sybe'+ x} value={x}>{x}</option>)}
                </FormControl>
            </FormGroup>
        </div>
       );


} 
}
export default connect(state => ({filters: state.filters} ))(SelectYearBuilt);