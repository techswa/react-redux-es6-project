import React from 'react';
import { connect } from "react-redux";
import { BrowserRouter, Route, NavLink } from 'react-router-dom';
import { Redirect } from 'react-router';

import { userLogin } from '../../actions/userActions';
import { userLogout } from '../../actions/userActions';

import { setUserEmail } from '../../actions/userActions';
import { setUserPassword } from '../../actions/userActions';


class LoginPage extends React.Component {

 setUserEmail = (value) => {
   console.log(value);
  this.props.dispatch(setUserEmail(value));
 }
  
 setUserPassword = (value) => {
  console.log(value);
  this.props.dispatch(setUserPassword(value));
}


  onSubmit(e) {
      e.preventDefault();
      
      this.props.dispatch(userLogin(this.props.user.user_email, this.props.user.user_password));
      
     };

  clickDisplayTickets = (e) => {
    e.preventDefault();
    this.props.history.push("/tickets");
  }

  clickLogout = (e) => {
    e.preventDefault();
    this.props.dispatch(userLogout());
    // alert("Logout");
  }
  
      loginForm = () => {
        return (
    
              <form className="" onSubmit={(e) => {this.onSubmit(e)}}>
              <div  className=""> 
                <legend>eMail</legend>
              </div>
                <div className="">
                  {/*<InputText className="" type="text" onChange={(e) => {this.setUserEmail(e.target.value)}} />*/}
                </div>
                <br/>
                <div className="">
                <legend>Password</legend>
                {/*<InputText className="" type="password" onChange={(e) => {this.setUserPassword(e.target.value)}} />*/}
              </div>
              <div>
                <button type="submit" className="btn-login">Login</button>
              </div>
              </form>
              
          );  
        };

      displayTickets1 = () => {
        return(       
            <div className="center"><NavLink to="/tickets" exact activeClassName="active">Display Tickets</NavLink></div>
        );  
      };  

      displayTickets = () => {
        return(
          <div>
            <div><button type="button" className="btn-display-tickets" onClick={(e) => {this.clickDisplayTickets(e)}}>Display Tickets</button></div>
            <br/>
            <div><button type="button" className="btn-display-tickets" onClick={(e) => {this.clickLogout(e)}}>Log out</button></div>
        </div>
        );
      }
      
      displayForm = () => {

        if(!this.props.user.user_id){
          return(
            <div>
            {this.loginForm()}                
            </div>
          );
        } else {

        return(
          <div>
          {this.displayTickets()}                
          </div>
        );
      }
      }
      
      render() {
    


  return (
    <div className="ui-g ui-fluid">
    <div className="login-form">
      <div className="center">
      <h1>User Login</h1>
      <br/>
      {this.displayForm()}
      </div>
      </div> 
      </div> 
    
  );
}
      }


export default connect(state => ({user: state.user, tickets: state.tickets}))(LoginPage);
