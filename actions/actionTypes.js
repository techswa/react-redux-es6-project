

//Refactored User
export const FETCH_USER_LOGIN                   = "FETCH_USER_LOGIN"
export const FETCH_USER_BUSY                    = "FETCH_USER_BUSY"
export const SET_USER_REJECTED                  = "SET_USER_REJECTED"
export const SET_USER_LOGOUT                    = "SET_USER_LOGOUT"
export const SET_USER_ID                        = "SET_USER_ID"
export const SET_USER_ROLE                      = "SET_USER_ROLE"
export const SET_USER_ACCESS_TOKEN              = "SET_USER_ACCESS_TOKEN"
export const SET_USER_EMAIL                     = "SET_USER_EMAIL"
export const SET_USER_PASSWORD                  = "SET_USER_PASSWORD"
export const SET_USER_META_DATA                 = "SET_USER_META_DATA"

//query constants
export const SET_FILTERS_TYPE                     = "SET_FILTERS_TYPE"
export const COUNT                              = "COUNT"
export const ALL                                = "ALL"
export const COUNTY                             = "COUNTY"
export const ZIPCODE                            = "ZIPCODE"

//Views
export const LIST_VIEW                         = "LIST_VIEW"
export const DETAIL_VIEW                        = "DETAIL_VIEW"
export const MAP_VIEW                        = "MAP_VIEW"
//Layout & page state  - currently kept in user
export const INDEX_LAYOUT                       = "INDEX_LAYOUT"
export const MINING_LAYOUT                      = "MINING_LAYOUT"
export const RESULTS_LAYOUT                     = "RESULTS_LAYOUT"
export const PROSPECTS_LAYOUT                   = "PROSPECTS_LAYOUT"
export const SET_CURRENT_LAYOUT                 = "SET_CURRENT_LAYOUT"
export const SET_CURRENT_LAYOUT_STYLES          = "SET_CURRENT_LAYOUT_STYLES"
export const SET_CURRENT_PAGE                   = "SET_CURRENT_PAGE"
export const SEARCH_PROPERTIES                  = "SEARCH_PROPERTIES"
export const PROPERTY_SEARCH                    = "PROPERTY_SEARCH"
export const PROPERTY_RESULTS_TABLE             = "PROPERTY_RESULTS_TABLE"
export const PROPERTY_RESULTS_SUMMARY           = "PROPERTY_RESULTS_SUMMARY"
export const QUERY_FORM_HORIZONTAL              = "QUERY_FORM_HORIZONTAL"

export const SET_PROSPECT_STATUS                   = "SET_PROSPECT_STATUS"
export const FETCH_PROSPECT_SUCCESS                = "FETCH_PROSPECT_SUCCESS" 
export const FETCH_PROSPECT_BUSY                   = "FETCH_PROSPECT_BUSY" 
export const FETCH_PROSPECT_REJECTED               = "FETCH_PROSPECT_REJECTED"

//Query Filter Types
export const FILTER_COUNT                    = "FILTER_COUNT"
export const FILTER_DETAIL                   = "FILTER_DETAIL"
// export const OWNER_NAME                      = "OWNER_NAME"
export const COMPANY_NAME                    = "COMPANY_NAME"
export const ESTATE_NAME                     = "ESTATE_NAME"

