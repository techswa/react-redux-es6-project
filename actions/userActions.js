
import { FETCH_USER_LOGIN } from './actionTypes';
import { FETCH_USER_BUSY } from './actionTypes';
import { SET_USER_REJECTED } from './actionTypes';
import { SET_USER_LOGOUT } from './actionTypes';
import { SET_USER_ID } from './actionTypes';
import { SET_USER_ROLE } from './actionTypes';
import { SET_USER_ACCESS_TOKEN } from './actionTypes';
import { SET_USER_EMAIL } from './actionTypes';
import { SET_USER_PASSWORD } from './actionTypes';
import { SET_USER_META_DATA } from './actionTypes';
import { SET_CURRENT_LAYOUT } from './actionTypes';

import axios from 'axios';
import getEnvironmentals from '../utils/load_enviromentals';
import { Redirect } from 'react-router';

console.log('Inside userAction');
export function setUserAccessToken(user_token){
    return {
        type: SET_USER_ACCESS_TOKEN,
        payload: user_token,
    }
}

export function setUserId(user_id){
    return {
        type: SET_USER_ID,
        payload: user_id,
    }
}

export function setCurrentLayout(layout){
    return {
        type: SET_CURRENT_LAYOUT,
        payload: layout,
    }
}

export function fetchUserBusy(state=true){
    return {
        type: FETCH_USER_BUSY,
        payload: true,
    }
}

export function setUserRejected(){
    return {
        type: SET_USER_REJECTED,
        payload: true,
    }
}

export function setUserEmail(email){
    return {
        type: SET_USER_EMAIL,
        payload: email,
    }
}
export function setUserPassword(password){
    return {
        type: SET_USER_PASSWORD,
        payload: password,
    }
}

export function setUserLogout(){

        return {
        type: SET_USER_LOGOUT,
        payload: '',
    }
    
}

export function fetchUserLogin(user_email, user_password){
    
        let query ={
            uri:'/api/v1/login',
            params: {
                username: user_email,
                password: user_password,
            } 
        }
        
        return getUserToken(query);
                 
    }

export function userKillToken(){
    return {
        type: SET_USER_ACCESS_TOKEN,
        payload: false,
        } 
    }
    

function getUserToken(query){
    let uri = query.uri;
    console.log('uri: ' + uri)
    let params = query.params;
    
    console.log("params: " + JSON.stringify(params));
   // Are we in dev or production mode    
    const env = getEnvironmentals();
    let url = env.server + uri;
    //  params = {ticket: "9682"};
    console.log('api_request: ' + url);
    // console.log('params: ' + JSON.stringify(params));


    return (dispatch) => {
        dispatch(fetchUserBusy(true));
        axios.get(uri, {
            auth: params
        })
          .then(function (response) {
              console.log(response.data);
            //Just send the web token for storage
            dispatch(setUserAccessToken(response.data.token));
            dispatch(setUserId(response.data.userId));
            //Load tickets
            
         })
          .catch(function (error) {
              console.log(error);
              //kill the current token
              dispatch(userKillToken());
            // dispatch(fetchUserRejected(true));

          });

        }
    }
